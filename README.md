# Pull Request 

Se realizaron 3 pull request en el repositorio Open Source <code> Python-for-beginner </code> ubicado en https://github.com/avsingh999/Python-for-beginner, se realizó el fork desde el repositorio de GitHub https://github.com/MaricheloGv/Python-for-beginner


## Información del repositorio Python-for-beginner

**Sobre el autor**
> Autor: [Avkaran Singh](https://www.linkedin.com/in/avkaran-singh-a1ba3613b/)
> Originario de: Gandhi Nagar, Gujarat, India
> Google Code In '18' Mentor

**Repositorio**
Este repostorio contiene un ejemplo simple, códigos en python . Ayudará a los principiantes a aprender python desde el principio.

_Cómo contribuir_
<br/>
Reclama el problema lo que quieras.
1. Busca un ejemplo o código que sea la mejor solución para ese problema
2. Crear un nombre de archivo de archivo debe ser el título del problema y pegar un código, ejemplo de ese problema
3. Crear una solicitud de extracción

# Realizar pull request a open source 1

1. Consistió en colaborar con las instrucciones y código para leer datos de arduino en Python para resolver el ISSUE #78
[How read arduino data in python.md](https://gitlab.com/MaryChelo/pull-request/blob/master/Evidencia-Pull%20Request_Python-for-beginner/how%20to%20read%20arduino%20data%20in%20python.md)

## Se ubicó un <code> issue </code> en el repostorio en cual trabajar para colaborar dentro del repositorio

![](Evidencia-Pull%20Request_Python-for-beginner/imagenesPull1/1-Desarrollando%20issue.png)<br/>

## Realizar commit 

![](Evidencia-Pull%20Request_Python-for-beginner/imagenesPull1/2-%20Add%20y%20commit.png)<br/>

## Comparando cambios

![](Evidencia-Pull%20Request_Python-for-beginner/imagenesPull1/3-Pull%20request%20Comparando%20cambios.png)

## Agregando archivo

![](Evidencia-Pull%20Request_Python-for-beginner/imagenesPull1/4-Pull%20request%20Archivo%20Agregado.png)<br/>

## Able merge

![](Evidencia-Pull%20Request_Python-for-beginner/imagenesPull1/5-Able%20merge.png)<br/>

## Enlazar al issue

![](Evidencia-Pull%20Request_Python-for-beginner/imagenesPull1/6-Enlace%20a%20issue.png)<br/>

## Solicitud de extracción <code> OPEN </code>

![](Evidencia-Pull%20Request_Python-for-beginner/imagenesPull1/7-%20Open%20Pull%20request.png)<br/>

## Solicitud de extracción <code> APPROVED </code>

![](Evidencia-Pull%20Request_Python-for-beginner/imagenesPull1/8-Pull%20request%20aceptado.png)<br/>

## Solicitud de extracción <code> MERGE </code>

![](Evidencia-Pull%20Request_Python-for-beginner/imagenesPull1/10-%20Merge%20pull%20request.png)<br/>

## PULL REQUEST <code> ADDED </code>

![](Evidencia-Pull%20Request_Python-for-beginner/imagenesPull1/11-%20Ya%20implementado%20en%20el%20repositorio.png)<br/>

## Contribuidor del repositorio
![](Evidencia-Pull%20Request_Python-for-beginner/imagenesPull1/9-Contribución.png)<br/>


# Realizar pull request a open source 2

2. Se realizó la coblaboración con un archivo para la introducción a los DataFrames y limpieza de datos de valores NaN.
[Introducing the DataFrame and Cleaning NaN values.md](https://gitlab.com/MaryChelo/pull-request/blob/master/Evidencia-Pull%20Request_Python-for-beginner/Introducing_DataFrame_and_Cleaning_NaN_values.md)

## Desarrollando tutorial

![](Evidencia-Pull%20Request_Python-for-beginner/imagenesPull2/1-%20Desarrollando%20md.png)<br/>

## Realizar commit 

![](Evidencia-Pull%20Request_Python-for-beginner/imagenesPull2/2-%20Commit.png)<br/>

## Comparando cambios

![](Evidencia-Pull%20Request_Python-for-beginner/imagenesPull2/3-%20Comparando.png)

## Abriendo una solicitud de extracción

![](Evidencia-Pull%20Request_Python-for-beginner/imagenesPull2/4%20-%20Pull.png)<br/>

**Se actualizó el archivo**

![](Evidencia-Pull%20Request_Python-for-beginner/imagenesPull2/6-%20Update.png)<br/>

# Realizar pull request a open source 3

3. Se realizó la colaboración con un tutorial para utilizar la librería tkinter de Python resolviendo así el issue #80 
[How to use tkinter library for python.md](https://gitlab.com/MaryChelo/pull-request/blob/master/Evidencia-Pull%20Request_Python-for-beginner/how%20to%20use%20tkinter%20library%20for%20python.md)

## Se ubicó un issue en el repostorio en cual trabajar para colaborar dentro del repositorio

![](Evidencia-Pull%20Request_Python-for-beginner/imagenesPull2/7%20-%20Comenzando%20nuevo%20pull.png)<br/>

## Realizar commit 

![](Evidencia-Pull%20Request_Python-for-beginner/imagenesPull2/8%20-%20commit.png)<br/>


## Able merge

![](Evidencia-Pull%20Request_Python-for-beginner/imagenesPull2/9%20-%20Able%20Merge.png)<br/>

## Solicitud de extracción <code> VERIFIED </code>

![](Evidencia-Pull%20Request_Python-for-beginner/imagenesPull2/10%20-%20Verificado.png)<br/>

## Solicitud de extracción <code> MERGE </code>

![](Evidencia-Pull%20Request_Python-for-beginner/imagenesPull2/10%20-%20Merge%20Pull.png)<br/>

## PULL REQUEST <code> ADDED </code>

![](Evidencia-Pull%20Request_Python-for-beginner/imagenesPull2/10%20-%20Repo_principal.png)<br/>

## Contribuidor 
![](Evidencia-Pull%20Request_Python-for-beginner/imagenesPull2/11%20-%20Contribuidor.png)<br/>

## Contribuciones finales
![](Evidencia-Pull%20Request_Python-for-beginner/imagenesPull2/12%20-%20Contribuciones_finales.png)<br/>

<br/>